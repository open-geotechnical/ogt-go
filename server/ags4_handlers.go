package server

import (
	//"fmt"

	"net/http"
	"time"

	"github.com/gorilla/mux"

	"gitlab.com/open-geotechnical/ogt-go/ags4"
)

// handles /ags4/units.*
func AxUnits(resp http.ResponseWriter, req *http.Request) {

	payload := MakePayload()

	payload["units"], payload["error"] = ags4.DD.GetUnits()

	SendAjaxPayload(resp, req, payload)
}

func AxInfo(resp http.ResponseWriter, req *http.Request) {
	payload := map[string]interface{}{
		"repos":      "https://bitbucket.org/daf0dil/ags-def-json",
		"version":    "0.1-alpha",
		"server_utc": time.Now().UTC().Format("2006-01-02 15:04:05"),
		"endpoints":  EndPoints,
	}
	SendAjaxPayload(resp, req, payload)
}

// handles /ags4/abbrs
func AxAbbrs(resp http.ResponseWriter, req *http.Request) {

	payload := MakePayload()
	payload["abbrs"], payload["error"] = ags4.DD.GetAbbrs()
	SendAjaxPayload(resp, req, payload)
}

// handles /ajax/ags4/abbr/<head_code>*
func AxAbbr(resp http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	payload := MakePayload()

	payload["abbr"], payload["error"] = ags4.DD.GetAbbr(vars["head_code"])

	SendAjaxPayload(resp, req, payload)
}

// handles /ags4/groups
func AxAgs4Groups(resp http.ResponseWriter, req *http.Request) {

	payload := MakePayload()

	payload["groups"], payload["groups_count"] = ags4.DD.GetGroupsList()

	SendAjaxPayload(resp, req, payload)
}

// handles /ags4/groups_map
func AxAgs4GroupsMap(resp http.ResponseWriter, req *http.Request) {

	payload := MakePayload()

	payload["groups_map"] = ags4.DD.GetGroupsMap()

	SendAjaxPayload(resp, req, payload)
}

// handles /ajax/ags4/group/{group_code}
func AxAgs4Group(resp http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	payload := MakePayload()

	payload["group"], payload["error"] = ags4.DD.GetGroup(vars["group_code"])

	SendAjaxPayload(resp, req, payload)
}

// handles /ajax/ags4/examples*
func AxExamples(resp http.ResponseWriter, req *http.Request) {

	payload := MakePayload()

	payload["examples"], payload["error"] = ags4.GetExamples()

	SendAjaxPayload(resp, req, payload)
}

// handles /ags/4/units.*
func AxParse(resp http.ResponseWriter, req *http.Request) {

	example := req.URL.Query().Get("example")

	payload := MakePayload()
	payload["example"] = example

	SendAjaxPayload(resp, req, payload)

}

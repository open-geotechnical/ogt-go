package server

import (
	//"fmt"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"gopkg.in/yaml.v2"
)

func MakePayload() map[string]interface{} {

	data := make(map[string]interface{})
	data["success"] = true // extjs4 quirk
	data["error"] = nil
	return data
}

// SendAjaxPayload writes payload to the response in requested format
func SendAjaxPayload(resp http.ResponseWriter, request *http.Request, payload interface{}) {

	// pretty returns indents data and readable (notably json) is ?pretty=1 in url
	pretty := true //request.URL.Query().Get("pretty") == "0"

	// Determine which encoding from the mux/router
	vars := mux.Vars(request)
	enc := vars["ext"]
	if enc == "" {
		enc = "json"
	}
	// TODO validate encoding and serialiser
	// eg yaml, json/js, html, xlsx, ags4,

	// TODO map[string] = encoding

	// Lets get ready to encode folks...
	var bites []byte
	var err error
	var contentType string = "text/plain"

	if enc == "yaml" {
		bites, err = yaml.Marshal(payload)
		contentType = "text/yaml"

	} else if enc == "json" || enc == "js" {
		if pretty {
			bites, err = json.MarshalIndent(payload, "", "  ")
		} else {
			bites, err = json.Marshal(payload)
		}
		contentType = "application/json"

	} else {
		bites = []byte("OOPs no `.ext` recognised ")
	}

	if err != nil {
		http.Error(resp, err.Error(), http.StatusInternalServerError)
		return
	}

	resp.Header().Set("Content-Type", contentType)
	resp.Write(bites)
}

var EndPoints = map[string]string{
	"/":           "Data and Sys information",
	"/ags4/all":   "AGS4: All data",
	"/ags4/units": "AGS4: Units",
}

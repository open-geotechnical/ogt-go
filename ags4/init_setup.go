package ags4

import "fmt"

var DataDictFilePath string = ""

func InitLoad(filePath string) error {

	fmt.Println("LOADING AGS$ DD")

	DD.FilePath = filePath
	return DD.LoadFromFile()
}

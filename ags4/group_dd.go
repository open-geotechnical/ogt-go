package ags4

import (
	"errors"
)

var ErrInvalidGroupCodeInput = errors.New("Invalid group code arg, must be four characters")
var ErrGroupCodeNotFound = errors.New("Group code not found in data dict")

// GroupDataDict is the data_dict of an ags4group
// from the data dictonary
type GroupDD struct {
	Class string `json:"class"`

	// The group CODE
	GroupCode string `json:"group_code"`

	// The groups simple description
	GroupDescription string `json:"group_description"`

	//GroupRequired string `json:"group_required"`

	// The parent group code
	Parent string `json:"parent"`
	// The child groups code
	Child string `json:"child"`

	Headings map[string]*HeadingDD `json:"headings"`
	Notes    []string              `json:"notes"`
}

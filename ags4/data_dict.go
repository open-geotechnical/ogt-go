package ags4

import (
	"encoding/json"
	"io/ioutil"
)

var DD *Ags4DataDict

func init() {
	DD = new(Ags4DataDict)
	DD.Groups = make([]*GroupDD, 0)
}

type Ags4DataDict struct {
	FilePath  string
	Groups    []*GroupDD
	GroupsMap map[string]*GroupDD ` json:"groups" `
}

func (me *Ags4DataDict) LoadFromFile() error {

	// read file
	bites, err := ioutil.ReadFile(me.FilePath)
	if err != nil {
		return err
	}

	// Read into memory
	//groupsMap := make(map[string]*GroupDataDict)
	err = json.Unmarshal(bites, &me)
	if err != nil {
		return err
	}

	// Success so update memory cache
	// var mutex = &sync.Mutex{}
	// mutex.Lock()
	// GroupsDataDictMap = groupsMap
	// mutex.Unlock()
	// TODO classes
	//for code, grp := range GroupsMap {

	//}
	return nil
}

func (me *Ags4DataDict) GetGroupsList() ([]*GroupDD, int) {
	vals := make([]*GroupDD, 0, len(me.GroupsMap))
	for _, v := range me.GroupsMap {
		vals = append(vals, v)
	}
	return vals, len(me.Groups)
}

func (me *Ags4DataDict) GetGroupsMap() map[string]*GroupDD {
	return me.GroupsMap
}

func (me *Ags4DataDict) GetGroup(grp_code string) (*GroupDD, bool) {
	grpdd, found := me.GroupsMap[grp_code]
	//if ok {
	//	return grpdd
	return grpdd, found
}

func (me *Ags4DataDict) GetAbbrs() (*GroupDD, bool) {
	grpdd, found := me.GroupsMap["grp_code"]
	//if ok {
	//	return grpdd
	return grpdd, found
}

func (me *Ags4DataDict) GetAbbr(head_code string) (*GroupDD, bool) {
	grpdd, found := me.GroupsMap[head_code]
	//if ok {
	//	return grpdd
	return grpdd, found
}

func (me *Ags4DataDict) GetUnits() (*GroupDD, bool) {
	grpdd, found := me.GroupsMap["grp_code"]
	//if ok {
	//	return grpdd
	return grpdd, found
}

package ogt

import "gitlab.com/open-geotechnical/ogt-go/ags4"

type GroupData struct {
	Class            string ` json:"class" 		yaml:"class" `
	GroupCode        string ` json:"group_code" yaml:"group_code" `
	GroupDescription string ` json:"group_description" yaml:"group_description" `

	Headings []Heading             ` json:"headings" yaml:"headings" `
	Data     []map[string]DataCell ` json:"data" yaml:"data" `
}

func (me GroupData) LoadDataDict() {

	// Check the definition exists and use it
	grpdd, ok := ags4.DD.GetGroup(me.GroupCode)
	if ok {
		me.GroupDescription = grpdd.GroupDescription
		me.Class = grpdd.Class
		//gdata.Valid = true
	}

}

package ogtags

import (
	"encoding/json"
	"io/ioutil"
	"sync"
)

// The AbbrsDataDictMap is lookup of the abbreviations in memory,
// and a lookup variable
// - The AGS data type is `PA` ie pick abbreviation
// - the map's key is the HEADING code eg SAMP_TYPE
// - pointing to the abbreviiation definition
// - Please create your own validation schema at will
//var AbbrsDataDictMap map[string]*AbbrItem

//func init() {
//	AbbrsDataDictMap = make(map[string]*AbbrItem)
//}

// Represents an Abbreviation item from a PA = pick abbr picklist)
// eg SAMP_TYPE = sample type
//   B = BUlk SAMPLE
//   CONC = Concrete Cube
//   W = Water
//type AbbrItem struct {
//	Code        string ` json:"abbr" `
//	Description string ` json:"abbr_description" `
//
//}

// Represents an abbreviations for the headcode, type PA
// type AbbrDataDict struct {
// 	HeadCode    string       ` json:"head_code" `

// 	Abbrs       []AbbrItem   ` json:"abbrs" `
// }

// Returns a list of abbreviations sorted by head_code
// func AbbrsDataDictItems() ([]*AbbrDataDict, error) {

// 	var keys []string
// 	for k := range AbbrItem {
// 		keys = append(keys, k)
// 	}
// 	sort.Strings(keys)

// 	abbrs := make([]*AbbrDataDict, 0, 0)
// 	for _, k := range keys {
// 		abbrs = append(abbrs, AbbrItem[k])
// 	}
// 	return abbrs, nil
// }

// Loads AGS abbreviations.json
func LoadAbbrsDDFromFile(file_path string) error {

	bites, err := ioutil.ReadFile(file_path)
	if err != nil {
		return err
	}
	abbrsddMap := make(map[string]*AbbrItem)
	err = json.Unmarshal(bites, &abbrsddMap)
	if err != nil {
		return err
	}
	var mutex = &sync.Mutex{}
	mutex.Lock()
	AbbrDataDictItem = abbrsddMap
	return nil
}

package main

import (
	"flag"

	//"gitlab.com/open-geotechnical/ogt-go/ogt"
	//"github.com/open-geotechnical/ogt-go/ogtags"

	"gitlab.com/open-geotechnical/ogt-go/server"
)

func main() {

	// TODO check listen is a valid address/port etc
	listen := flag.String("server", "0.0.0.0:13777", "HTTP server address and port")

	ags4DdPath := flag.String("data-dict", "/home/ogt/ags4-data-dict/_build/spec/ags4.json", "Path to data dict dir")

	flag.Parse()

	// Initialise the datadict stores
	server.InitLoad(*ags4DdPath)

	// TODO make server a flag, for now its on for fun
	if true {
		server.Start(*listen)
	}

}
